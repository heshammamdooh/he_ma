'use strict';

class AppHeader extends HTMLElement { 

    constructor() {
        super();
    }

    createdCallback() {
        this.classList.add(...AppHeader.CLASSES);
        this.innerHTML = AppHeader.TEMPLATE;
    }

    attributeChangedCallback(attributeName) {
    }
}

// Initial content of the element.
AppHeader.TEMPLATE = `
    <div class="header__title mdl-card__title">
        <button class="header__button--cover-photo mdl-button mdl-js-button">
            <i class="icon ion-md-camera"></i> Upload cover photo
        </button>
    </div>

    <div class="header__details mdl-card__supporting-text mdl-grid">
        <div class="header__details--image-container mdl-cell mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
            <div class="header__image--profile mdl-shadow--2dp">
                <!-- image will render here via css -->
            </div>
        </div>
        <div class="header__info mdl-cell mdl-cell--12-col-phone mdl-cell--8-col-tablet mdl-cell--9-col-desktop">
            <p class="header__info--name"></p>
            <p class="header__info--location"><i class="icon ion-md-pin"></i><span></span></p>
            <p class="header__info--number"><i class="icon ion-md-call"></i><span></span></p>
        </div>
        <div class="header__review mdl-cell mdl-cell--12-col-phone mdl-cell--8-col-tablet mdl-cell--3-col-desktop">
            <div class="header__review--stars">
                <i class="icon "></i>
                <i class="icon "></i>
                <i class="icon "></i>
                <i class="icon "></i>
                <i class="icon "></i>
            </div>
            <div class="header__review--count">
                
            </div>
        </div>
    </div>

    <div class="header__menus mdl-card__actions mdl-card--border">
        <div class="header__menus--buttons">
            <a href="#about-tab" class="mdl-button mdl-js-button mdl-button--accent active">
                About
            </a>
            <a href="#settings-tab" class="mdl-button mdl-js-button mdl-button--accent">
                Settings
            </a>
            <a href="#option1-tab" class="mdl-button mdl-js-button mdl-button--accent">
                Option1
            </a>
            <a href="#option2-tab" class="mdl-button mdl-js-button mdl-button--accent">
                Option2
            </a>
            <a href="#option3-tab" class="mdl-button mdl-js-button mdl-button--accent">
                Option3
            </a>
        </div>
        <div class="header__menus--followers">
            <button class="followers__button--add mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab">
                <i class="material-icons">add</i>
            </button>
            <span class="followers-count">
                <span class="followers-count__number">15</span>
                <span class="followers-count__text">followers</span>
            </span>
        </div>
    </div>
    <div class="mdl-card__menu">
        <button class="header__button--logout mdl-button mdl-js-button">
            LOG OUT
        </button>
    </div>
    `;

AppHeader.CLASSES = ['header', 'demo-card-wide', 'mdl-card', 'mdl-shadow--2dp'];

document.registerElement('app-header', AppHeader);