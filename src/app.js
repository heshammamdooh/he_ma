import { AppHeader } from "./Header";
import { AppContent } from "./Content";

class MyApp {
    constructor() {
        this.xDown = null;
        this.yDown = null;
        this.user = this.getUserInfo();

        this.headerUserName = document.querySelector('.header__info--name');
        this.headerUserLocation = document.querySelector('.header__info--location span');
        this.headerUserNumber = document.querySelector('.header__info--number span');
        this.reviewsCount = document.querySelector('.header__review--count');
        this.reviewsStars = document.querySelectorAll('.header__review--stars>.icon');
        this.nameField = document.querySelector('.field-js-name > .details-field__value--field');
        this.webField = document.querySelector('.field-js-web > .details-field__value--field');
        this.numberField = document.querySelector('.field-js-number > .details-field__value--field');
        this.locationField = document.querySelector('.field-js-location > .details-field__value--field');

        this.mobileEditButton = document.querySelector('.tab-content__button--mobile-edit');
        this.mobileFormCanelButton = document.querySelector('.mobile-form-actions__button--cancel');
        this.mobileFormSaveButton = document.querySelector('.mobile-form-actions__button--save');

        this.tabContents = document.querySelectorAll('.content > .tab-content');
        this.tabButtons = document.querySelectorAll('.header__menus--buttons > a');
        this.desktopEditField = document.querySelectorAll('.details-field__value--action');
        this.popoverCanelButton = document.querySelectorAll('.details-field__popover__button--cancel');
        this.popoverSaveButton = document.querySelectorAll('.details-field__popover__button--save');

        this.registerMobileEvents();
        this.registerDesktopEvents();
        this.updateHeader(this.user);
        this.updateContent(this.user);
    }

    updateHeader(user) {
        this.headerUserName.textContent = `${user.first_name} ${user.last_name}`;
        this.headerUserLocation.textContent = `${user.location}`;
        this.headerUserNumber.textContent = `${user.number}`;
        this.reviewsCount.textContent = `${user.viewsCount} Reviews`;
        this.calcStars(this.reviewsStars, user.stars);
    }

    updateContent(user) {
        this.nameField.textContent = `${user.first_name} ${user.last_name}`;
        this.webField.textContent = user.web;
        this.numberField.textContent = user.number;
        this.locationField.textContent = user.location;
    }

    registerMobileEvents() {
        this.mobileEditButton.addEventListener('click', this.showMoblieForm.bind(this));
        this.mobileFormCanelButton.addEventListener('click', this.hideMoblieForm.bind(this));
        this.mobileFormSaveButton.addEventListener('click', this.saveMobileForm.bind(this));
    }

    registerDesktopEvents() {
        for (let tab of this.tabContents) {
            tab.addEventListener('touchstart', this.handleTouchStart.bind(this), false);
            tab.addEventListener('touchmove', this.handleTouchMove.bind(this), false);
        }

        for (let button of this.tabButtons) {
            button.addEventListener('click', this.activeTabContent.bind(this));
        }

        for (var i = 0; i < this.desktopEditField.length; i++) {
            this.desktopEditField[i].addEventListener('click', this.showDesktopPopover.bind(this));
        }

        for (var i = 0; i < this.popoverCanelButton.length; i++) {
            this.popoverCanelButton[i].addEventListener('click', this.hideDesktopPopover.bind(this));
        }

        for (var i = 0; i < this.popoverSaveButton.length; i++) {
            this.popoverSaveButton[i].addEventListener('click', this.savePopoverValue.bind(this));
        }
    }

    activeTabContent(evt) {
        let tabID = evt.target.href.split('#')[1];

        this.showTab(tabID);

        evt.preventDefault();
    }

    showTab(id) {
        let targetedBtn;

        this.tabContents.forEach((tab) => {
            tab.classList.remove('active');
        });
        document.getElementById(id).classList.add('active');

        this.tabButtons.forEach((button) => {
            if (button.href.split('#')[1] === id) targetedBtn = button;
            button.classList.remove('active');
        });
        targetedBtn.classList.add('active');
        targetedBtn.focus();
    }

    handleSwipAction(evt, direction) {
        let tabContent = evt.target.closest('.tab-content');
        let tabID = tabContent.id;

        if (direction === 'right') {
            if (tabID === this.tabContents[0].id)
                this.showTab(this.tabContents[this.tabContents.length - 1].id);
            else
                this.showTab(tabContent.previousElementSibling.id);
        } else if (direction === 'left') {
            if (tabID === this.tabContents[this.tabContents.length - 1].id)
                this.showTab(this.tabContents[0].id);
            else
                this.showTab(tabContent.nextElementSibling.id);
        }
    }

    handleTouchStart(evt) {
        this.xDown = evt.touches[0].clientX;
        this.yDown = evt.touches[0].clientY;
    }

    handleTouchMove(evt) {
        if (!this.xDown || !this.yDown) {
            return;
        }

        let xUp = evt.touches[0].clientX;
        let yUp = evt.touches[0].clientY;

        let xDiff = this.xDown - xUp;
        let yDiff = this.yDown - yUp;

        if (Math.abs(xDiff) > Math.abs(yDiff)) {/*most significant*/
            if (xDiff > 0) {
                /* left swipe */
                this.handleSwipAction(evt, 'left');
            } else {
                /* right swipe */
                this.handleSwipAction(evt, 'right');
            }
        }

        /* reset values */
        this.xDown = null;
        this.yDown = null;
    }

    showMoblieForm(evt) {
        let fields = evt.target.closest('.tab-content').querySelectorAll('.mobile-form .mobile-form-field input');

        for (var i = 0; i < fields.length; i++) {
            fields[i].value = this.user[fields[i].getAttribute('name')];
            fields[i].parentNode.classList.add('is-dirty');
        }

        evt.target.closest('.details').classList.add('hide');
        evt.target.closest('.tab-content').querySelector('.mobile-form').classList.remove('hide');
    }

    hideMoblieForm(evt) {
        evt.target.closest('.mobile-form').classList.add('hide');
        evt.target.closest('.tab-content').querySelector('.details').classList.remove('hide');
    }

    saveMobileForm(evt) {
        let formFields = evt.target.closest('.tab-content').querySelectorAll('.mobile-form .mobile-form-field input');
        let viewFields = Array.from(evt.target.closest('.tab-content').querySelectorAll('.details .details-field .details-field__value--field'));

        for (var i = 0; i < formFields.length; i++) {
            this.user[formFields[i].getAttribute('name')] = formFields[i].value;

            if (formFields[i].getAttribute('name') === 'first_name') {
                viewFields.find(field => field.getAttribute('for') === 'name').textContent = formFields[i].value;
            } else if (formFields[i].getAttribute('name') === 'last_name') {
                viewFields.find(field => field.getAttribute('for') === 'name').textContent += ' ' + formFields[i].value;
            } else {
                viewFields.find(field => field.getAttribute('for') === formFields[i].getAttribute('name')).textContent = formFields[i].value;
            }
        }

        this.hideMoblieForm(evt);
        this.updateHeader(this.user);
    }

    showDesktopPopover(evt) {
        let fieldType = evt.target.closest('.details-field__value').querySelector('.details-field__value--field').getAttribute('for');
        let popover = evt.target.closest('.details-field').querySelector('.details-field__popover');
        let popoverInput = evt.target.closest('.details-field').querySelector('.details-field__popover .mdl-textfield__input');

        if (fieldType !== 'name') {
            popoverInput.value = this.user[fieldType];
        } else {
            popoverInput.value = `${this.user['first_name']} ${this.user['last_name']}`;
        }

        popoverInput.parentNode.classList.add('is-dirty');
        popover.classList.add('show');
    }

    hideDesktopPopover(evt) {
        evt.target.closest('.details-field__popover').classList.remove('show');
    }

    savePopoverValue(evt) {
        let popoverInput = evt.target.closest('.details-field__popover').querySelector('.mdl-textfield__input');
        let viewField = evt.target.closest('.details-field').querySelector('.details-field__value--field');

        viewField.textContent = popoverInput.value;
        if (popoverInput.getAttribute('name') !== 'name') {
            this.user[popoverInput.getAttribute('name')] = popoverInput.value;
        } else {
            this.user['first_name'] = popoverInput.value.split(' ')[0];
            this.user['last_name'] = popoverInput.value.split(' ')[1];
        }

        this.hideDesktopPopover(evt);
        this.updateHeader(this.user);
    }

    calcStars(starsParent, count) {
        for (let i = 0; i < starsParent.length; i++) {
            if (i < count)
                starsParent[i].classList = ['ion-md-star'];
            else
                starsParent[i].classList = ['ion-md-star-outline'];
        }
    }

    getUserInfo() {
        return {
            first_name: 'Joan',
            last_name: 'Doe',
            location: 'Newport Beach, CA',
            web: 'www.seller.com',
            number: '(949) 325-68594',
            stars: 4,
            viewsCount: 16
        }
    }
}

window.addEventListener('load', () => new MyApp());