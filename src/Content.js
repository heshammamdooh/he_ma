'use strict';

class AppContent extends HTMLElement {

    constructor() {
        super();
    }

    createdCallback() {

        this.classList.add(...AppContent.CLASSES);
        this.innerHTML = AppContent.TEMPLATE;

    }

    attributeChangedCallback(attributeName) {
    }
}

AppContent.TEMPLATE = `
    <div class="tab-content" id="about-tab">
        <h6 class="tab-content__header">About</h6>

        <div class="details">
            <button class="tab-content__button--mobile-edit mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab">
                <i class="icon ion-md-create"></i>
            </button>

            <div class="details-field">
                <div class="details-field__value field-js-name">
                    <span class="details-field__value--field" for="name"></span>
                    <span class="details-field__value--action">
                        <i class="icon ion-md-create"></i>
                    </span>
                </div>
                
                <div class="details-field__popover">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="text" name="name">
                        <label class="mdl-textfield__label" for="sample3">Name</label>
                    </div>
                    <button class="details-field__popover__button--save mdl-button mdl-js-button mdl-js-ripple-effect">
                        save
                    </button>
                    <button class="details-field__popover__button--cancel mdl-button mdl-js-button mdl-js-ripple-effect">
                        cancel
                    </button>
                </div>
            </div>

            <div class="details-field">
                <div class="details-field__value field-js-web">
                    <span class="details-field__value--icon">
                        <i class="icon ion-md-globe"></i>
                    </span>
                    <span class="details-field__value--field" for="web"></span>
                    <span class="details-field__value--action">
                        <i class="icon ion-md-create"></i>
                    </span>
                </div>
                
                <div class="details-field__popover">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="text" name="web">
                        <label class="mdl-textfield__label" for="sample3">Web</label>
                    </div>
                    <button class="details-field__popover__button--save mdl-button mdl-js-button mdl-js-ripple-effect">
                        save
                    </button>
                    <button class="details-field__popover__button--cancel mdl-button mdl-js-button mdl-js-ripple-effect">
                        cancel
                    </button>
                </div>
            </div>

            <div class="details-field">
                <div class="details-field__value field-js-number">
                    <span class="details-field__value--icon">
                        <i class="icon ion-md-call"></i>
                    </span>
                    <span class="details-field__value--field" for="number"></span>
                    <span class="details-field__value--action">
                        <i class="icon ion-md-create"></i>
                    </span>
                </div>
                
                <div class="details-field__popover">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="text" name="number">
                        <label class="mdl-textfield__label" for="sample3">Number</label>
                    </div>
                    <button class="details-field__popover__button--save mdl-button mdl-js-button mdl-js-ripple-effect">
                        save
                    </button>
                    <button class="details-field__popover__button--cancel mdl-button mdl-js-button mdl-js-ripple-effect">
                        cancel
                    </button>
                </div>
            </div>

            <div class="details-field">
                <div class="details-field__value field-js-location">
                    <span class="details-field__value--icon">
                        <i class="icon ion-md-home"></i>
                    </span>
                    <span class="details-field__value--field" for="location"></span>
                    <span class="details-field__value--action">
                        <i class="icon ion-md-create"></i>
                    </span>
                </div>
                
                <div class="details-field__popover">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="text" name="location">
                        <label class="mdl-textfield__label" for="sample3">City, State & zip</label>
                    </div>
                    <button class="details-field__popover__button--save mdl-button mdl-js-button mdl-js-ripple-effect">
                        save
                    </button>
                    <button class="details-field__popover__button--cancel mdl-button mdl-js-button mdl-js-ripple-effect">
                        cancel
                    </button>
                </div>
            </div>
        </div>

        <div class="mobile-form hide">
            <div class="mobile-form-actions">
                <button class="mobile-form-actions__button--cancel mdl-button mdl-js-button mdl-js-ripple-effect">
                    cancel
                </button>
                <button class="mobile-form-actions__button--save mdl-button mdl-js-button mdl-js-ripple-effect">
                    save
                </button>
            </div>

            <div class="mobile-form-field mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="first_name">
                <label class="mdl-textfield__label" for="sample3">First name</label>
            </div>

            <div class="mobile-form-field mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="last_name">
                <label class="mdl-textfield__label" for="sample3">Last name</label>
            </div>

            <div class="mobile-form-field mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="web">
                <label class="mdl-textfield__label" for="sample3">Website</label>
            </div>

            <div class="mobile-form-field mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="number">
                <label class="mdl-textfield__label" for="sample3">Phone number</label>
            </div>

            <div class="mobile-form-field mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="location">
                <label class="mdl-textfield__label" for="sample3">City, state & zip</label>
            </div>
        </div>
    </div>

    <div class="tab-content" id="settings-tab">
        <h6 class="tab-content__header">Settings</h6>
    </div>

    <div class="tab-content" id="option1-tab">
        <h6 class="tab-content__header">Option1</h6>
    </div>

    <div class="tab-content" id="option2-tab">
        <h6 class="tab-content__header">Option2</h6>
    </div>

    <div class="tab-content" id="option3-tab">
        <h6 class="tab-content__header">Option3</h6>
    </div>
`;

AppContent.CLASSES = ['content', 'demo-card-wide', 'mdl-card', 'mdl-shadow--2dp'];

document.registerElement('app-content', AppContent);